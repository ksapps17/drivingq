# DrivingQ

This is a prototype I built in 2016 in Xcode 9 with Swift 3, revitalized for Xcode 10.1 in Swift4.2. The app contains Swift and Objective-C libraries from several attributed sources on the info page.

The app helps students learning to research to create a driving question that they then have to prove as part of a project-based research program. From an idea from Seth Jaeger, an educational Ph.D. student. A "driving question" is created on a "topic", say: "football" in which four expressions are chosen from the segmented controller, one-at-at-time and so compose the question: stem, concept, category & verb. The resultant question can be the motivation for thoughts that drive research in unexpected directions when used as an axoim or premise. It is only used for educational purposes in high schools.

This version is buggy because the mail and website buttons don't work in the InfoController because I made changes to my website.

To use: 1) Agree to terms, 2) use the 'ooo' menu bar item, 3) read instructions, then 4) hit 'Got It!' and a question will generate.

Copyright 2016 ksapps llc. All rights reserved.
